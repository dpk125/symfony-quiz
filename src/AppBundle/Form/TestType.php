<?php

namespace AppBundle\Form;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
            'answer',
            ChoiceType::class,
            [
                'mapped' => false,
                'choices' => $builder->getData()->getAnswers(),
                'multiple' => false,
                'expanded' => true,
                'choice_label' => function (Answer $answer) {
                    return $answer->getName();
                },
            ]
        );
    }

    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        parent::finishView($view, $form, $options);

        $submission = $form->get('answer')->getData();

        if (null === $submission) {
            return;
        }

        $answersCollection = $form->getData()->getAnswers();

        foreach ($view->children['answer']->children as $answer) {
            $value = $answer->vars['value'];
            /** @var Answer $data */
            $data = $answersCollection->get($value);

            $answer->vars['wrong'] = $answer->vars['checked'] && false === $data->isCorrect();
            $answer->vars['correct'] = $data->isCorrect();
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(
                [
                    'data_class' => Question::class,
                ]
            );
        ;
    }
}
