<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Question;
use Doctrine\ORM\EntityRepository;

class QuestionRepository extends EntityRepository
{
    public function findWithAnswers(int $id): ?Question
    {
        $qb = $this->createQueryBuilder('q');

        $qb
            ->select(['q', 'a'])
            ->innerJoin('q.answers', 'a')
            ->where($qb->expr()->eq('q', ':id'))
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }
}