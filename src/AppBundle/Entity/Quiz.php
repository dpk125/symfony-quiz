<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Quiz
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Collection|Question[]
     */
    private $questions;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Quiz
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): Quiz
    {
        if (false === $this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setQuiz($this);
        }

        return $this;
    }

    public function getNextQuestion(Question $question): ?Question
    {
        $index = $this->questions->indexOf($question);

        if ($index === $this->questions->count() - 1) {
           return null;
        }

        return $this->questions->get($index + 1);
    }

    public function getPreviousQuestion(Question $question): ?Question
    {
        $index = $this->questions->indexOf($question);

        if ($index === 0) {
            return null;
        }

        return $this->questions->get($index - 1);
    }

    public function removeQuestion(Question $question): Quiz
    {
        $this->questions->removeElement($question);

        return $this;
    }
}
