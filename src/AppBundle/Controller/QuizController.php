<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Question;
use AppBundle\Entity\Quiz;
use AppBundle\Form\QuizType;
use AppBundle\Form\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuizController extends Controller
{
    /**
     * @Route(name="quiz_index", path="/")
     */
    public function indexAction(): Response
    {
        $quizRepository = $this->getDoctrine()->getRepository('AppBundle:Quiz');
        $quizes = $quizRepository->findAll();

        return $this->render(
            'quiz/index.html.twig',
            [
                'quizes' => $quizes,
            ]
        );
    }

    /**
     * @Route(name="quiz_create", path="/create")
     */
    public function createAction(Request $request): Response
    {
        $quiz = new Quiz();

        $form = $this->createForm(QuizType::class, $quiz);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($quiz);
            $em->flush();

            return $this->redirectToRoute('quiz_index');
        }

        return $this->render(
            ':quiz:create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(name="quiz_edit", path="/{quiz}/edit")
     */
    public function editAction(Quiz $quiz, Request $request): Response
    {
        $form = $this->createForm(QuizType::class, $quiz);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('quiz_index');
        }

        return $this->render(
            ':quiz:edit.html.twig',
            [
                'quiz' => $quiz,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(name="quiz_delete", path="/{quiz}/delete")
     */
    public function deleteAction(Quiz $quiz): Response
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($quiz);
        $em->flush();

        return $this->redirectToRoute('quiz_index');
    }

    /**
     * @Route(name="quiz_take", path="/{quiz}/take/{question}")
     */
    public function takeAction(Quiz $quiz, $question, Request $request): Response
    {
        $repository = $this->getDoctrine()->getRepository(Question::class);
        $question = $repository->findWithAnswers($question);

        $form = $this->createForm(TestType::class, $question);
        $form->handleRequest($request);

        return $this->render(
            ':quiz:check.html.twig',
            [
                'form' => $form->createView(),
                'question' => $question,
                'quiz' => $quiz,
            ]
        );
    }
}
