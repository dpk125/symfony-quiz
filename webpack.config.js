var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('web/assets/')
    .setPublicPath('/assets')
    .cleanupOutputBeforeBuild()
    .addEntry('app', './resources/js/main.js')
    .addEntry('pages/quiz', './resources/js/pages/quiz/form.js')
    .addStyleEntry('main', './resources/css/main.scss')
    .enableSassLoader(function(sassOptions) {}, {
        resolve_url_loader: false
    })
    // .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction())

// create hashed filenames (e.g. app.abc123.css)
// .enableVersioning()
;

module.exports = Encore.getWebpackConfig();
