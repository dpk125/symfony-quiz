# Quiz application

1. Install dependecies `composer install`
2. Initialize database `bin/console doctrine:database:create`
3. Install assets with `yarn run encore dev`
4. Start server `bin/console server:start`
5. Go to <http://127.0.0.1:8000>

## TODO

* Validation
    * Set novalidate parameter on forms
    * Quiz
    * Answer
    * One correct answer
